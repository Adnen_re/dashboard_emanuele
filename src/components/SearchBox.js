import React from "react";
import Input from "./shared/Input";
import ButtonIcon from "./shared/ButtonIcon";
const SearchBox = () => {
  return (
    <div className="searchBox">
      <Input className="searchBox_input" />
      <ButtonIcon />
    </div>
  );
};
export default SearchBox;
