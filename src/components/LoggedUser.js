import React from "react";
import Avatar from "./shared/Avatar";

const User = ({ image, name }) => (
  <div className="user-detail">
    <Avatar src={image} />
    <p>{name}</p>
  </div>
);
export default User;
