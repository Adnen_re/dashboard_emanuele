import React from "react";
import DropDown from "./shared/DropDown";
import User from "./LoggedUser";
const UserPanel = props => {
  const { name, image, messages, notifications } = props.data;
  return (
    <div className="userPanel">
      <DropDown
        icon={"fa fa-bell"}
        elements={notifications}
        {...props}
        badgeNum={notifications.length}
      />
      <DropDown
        icon={"fa fa-comments-o"}
        elements={messages}
        badgeNum={messages.length}
        {...props}
      />

      <User name={name} image={image} />
    </div>
  );
};

export default UserPanel;
