import React from "react";
import PropTypes from "prop-types";

const Button = props => {
  const { className, onClick, value, children } = props;
  return (
    <button className={className} onClick={onClick}>
      {!value ? children : value}
    </button>
  );
};
Button.propTypes = {
  className: PropTypes.string,
  value: PropTypes.string,
  onClick: PropTypes.func,
  children: PropTypes.any
};
Button.defaultProps = {
  className: "btn btn-default",
  value: "",
  children: null,
  onClick: null
};
export default Button;
