import React from "react";

const Badge = props => {
  return (
    <span className="badge" {...props}>
      {props.value}
    </span>
  );
};
export default Badge;
