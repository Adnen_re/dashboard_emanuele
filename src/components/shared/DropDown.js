import React from "react";
import ButtonIcon from "./ButtonIcon";
import Badge from "./Badge";
class DropDown extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false
    };
    this.toggleDropDown = this.toggleDropDown.bind(this);
  }

  toggleDropDown() {
    this.setState({ isOpen: !this.state.isOpen });
  }
  render() {
    return (
      <div className="dropdown">
        <ButtonIcon
          className="dropdown_btn"
          onClick={this.toggleDropDown}
          {...this.props}
        />
        <Badge value={this.props.badgeNum} />
        <ul
          className={
            "dropdown_menu " +
            (this.state.isOpen ? "dropdown_menu--active" : "")
          }>
          {this.props.elements.map((el, i) => {
            return (
              <React.Fragment>
                <span
                  className={
                    "badge_" + (el.user ? "message" : "notif " + el.type)
                  }
                  key={i}>
                  {el.user ? el.user.slice(0, 1).toUpperCase() : el.type}
                </span>
                <li key={i}>{el.content.slice(0, 10) + "..."}</li>
              </React.Fragment>
            );
          })}
        </ul>
      </div>
    );
  }
}

export default DropDown;
