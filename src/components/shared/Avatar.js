import React from "react";
import PropTypes from "prop-types";
const Avatar = props => {
  const { src } = props;
  return (
    <div className="avatar">
      <img src={src} alt="user name" />
    </div>
  );
};

Avatar.propTypes = {
  src: PropTypes.string.isRequired
};

export default Avatar;
