import React from "react";
import PropTypes from "prop-types";
import Button from "./Button";
const ButtonIcon = props => {
  return (
    <Button onClick={props.onClick} {...props}>
      <i className={props.icon} />
    </Button>
  );
};
ButtonIcon.propTypes = {
  icon: PropTypes.string
};
ButtonIcon.defaultProps = {
  icon: "fa fa-search"
};
export default ButtonIcon;
