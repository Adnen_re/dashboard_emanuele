import React from "react";

const Logo = () => {
  return (
    <svg width="101" height="55" xmlns="http://www.w3.org/2000/svg">
      <g>
        <rect
          fill="#fff"
          id="canvas_background"
          height="57"
          width="103"
          y="-1"
          x="-1"
        />
        <g
          display="none"
          overflow="visible"
          y="0"
          x="0"
          height="100%"
          width="100%"
          id="canvasGrid">
          <rect
            fill="url(#gridpattern)"
            y="0"
            x="0"
            height="100%"
            width="100%"
          />
        </g>
      </g>
      <g>
        <path
          stroke="#ff007f"
          id="svg_2"
          d="m15.812498,44.18555c0,0 50.988514,-0.100196 50.937526,-0.14873c0.050989,0.048535 -16.877198,-23.196853 -16.928187,-23.245388c0.050989,0.048535 -14.42975,23.193727 -14.42975,23.193727c0,0 20.395406,0 20.344417,-0.048535c0.050989,0.048535 -23.301751,-31.513091 -23.35274,-31.561626c0.050989,0.048535 -16.571267,31.810552 -16.571267,31.810552z"
          strokeWidth="1.5"
          fill="#fff"
        />
      </g>
    </svg>
  );
};
export default Logo;
