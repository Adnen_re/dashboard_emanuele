import React, { useState } from "react";
import PropTypes from "prop-types";

const SearchBox = props => {
  const [value, setValue] = useState("");
  const { placeholder, className } = props;
  return (
    <input
      className={className}
      placeholder={placeholder}
      type="text"
      value={value}
      onChange={e => setValue(e.target.value)}
    />
  );
};
SearchBox.propTypes = {
  className: PropTypes.string,
  placeholder: PropTypes.string,
  onChange: PropTypes.func
};
SearchBox.defaultProps = {
  placeholder: "Search...",
  className: null,
  onChange: null
};
export default SearchBox;
