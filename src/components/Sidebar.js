import React from "react";
import { NavLink } from "react-router-dom";

class Sidebar extends React.PureComponent {
  constructor(props) {
    super(props);
    this.activeRoute.bind(this);
  }
  // verifies if routeName is the one active (in browser input)
  activeRoute(routeName) {
    return this.props.location.pathname.indexOf(routeName) > -1 ? "active" : "";
  }

  render() {
    return (
      <div className={this.props.className}>
        <div className="sidebar-wrapper">
          <ul>
            {this.props.routes.map((prop, i) => {
              return (
                <li
                  className={
                    this.activeRoute(prop.path) +
                    (prop.pro ? " active-pro" : "")
                  }
                  key={i}>
                  <NavLink
                    to={prop.layout + prop.path}
                    className="nav-link"
                    activeClassName="active">
                    <i className={prop.icon} />
                    <p>{prop.name}</p>
                  </NavLink>
                </li>
              );
            })}
          </ul>
        </div>
        <span className="copyright">Copyright 2018</span>
      </div>
    );
  }
}

export default Sidebar;
