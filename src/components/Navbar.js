import React from "react";
import Logo from "./shared/Logo";
import SearchBox from "./SearchBox";
import UserPanel from "./UserPanel";
const Navbar = props => {
  const { data } = props;
  console.log(data);
  return (
    <div className="navbar">
      <Logo />
      <SearchBox />
      <UserPanel data={data} />
    </div>
  );
};

export default Navbar;
