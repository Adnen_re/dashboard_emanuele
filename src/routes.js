import Dashboard from './view/Dashboard';

const routes = [
  {
    path: "/flights",
    name: "Flights",
    icon: "nav-link_icon fa fa-genderless ",
    component: Dashboard,
    layout: "/admin"
  },
  {
    path: "/hotels",
    name: "Hotels",
    icon: "nav-link_icon fa fa-genderless ",
    component: Dashboard,
    layout: "/admin"
  },
  {
    path: "/renting",
    name: "Renting",
    icon: "nav-link_icon fa fa-genderless ",
    component: Dashboard,
    layout: "/admin"
  },
  {
    path: "/contact",
    name: "Contact",
    icon: "nav-link_icon fa fa-genderless ",
    component: Dashboard,
    layout: "/admin"
  },
  {
    path: "/about-us",
    name: "About us",
    icon: "nav-link_icon fa fa-genderless ",
    component: Dashboard,
    layout: "/admin"
  }
];

export default routes;
