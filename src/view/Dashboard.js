import React from "react";
import routes from "../routes";
const getRoutName = () => {
  let name = "Default";
  routes.map(prop => {
    if (window.location.href.indexOf(prop.layout + prop.path) !== -1) {
      name = prop.name;
    }
    return null;
  });
  return name;
};
const Dashboard = () => {
  return <div>{getRoutName()}</div>;
};
export default Dashboard;
