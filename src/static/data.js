import userImage from "./user.png";

const data = {
  name: "Emanuele",
  image: userImage,
  messages: [
    {
      id: 1,
      content: "hello how are you ",
      user: "Jhone"
    },
    {
      id: 2,
      content: "Can you send me the link ",
      user: "Mery"
    },

    {
      id: 3,
      content: "Can you send me the link ",
      user: "eline"
    },

    {
      id: 4,
      content: "Hi there can you check out ",
      user: "mike"
    },

    {
      id: 5,
      content: "i send you that message ... ",
      user: "jackque"
    }
  ],
  notifications: [
    {
      id: 1,
      content: "Your laptop need... ",
      type: "system"
    },
    {
      id: 2,
      content: "email for mery ",
      type: "email"
    },
    {
      id: 3,
      content: "VM need to free memory ",
      type: "system"
    }
  ]
};
export default data;
