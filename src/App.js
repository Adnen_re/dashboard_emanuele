import React from "react";
import { createBrowserHistory } from "history";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import "./App.css";

import AdminDashboard from "./layouts/Admin";

const history = createBrowserHistory();

const App = () => {
  return (
    <Router history={history}>
      <Switch>
        <Route path="/" render={props => <AdminDashboard {...props} />} />
        <Redirect to="/dashboard" />
      </Switch>
    </Router>
  );
};

export default App;
