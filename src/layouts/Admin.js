import React from "react";
import { Route, Switch } from "react-router-dom";
import Navbar from "../components/Navbar";
import Sidebar from "../components/Sidebar";
import routes from "../routes";
// fake user data
import data from "../static/data.js";
class AdminDashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {}
    };
  }
  componentWillMount() {
    this.setState({ data });
  }
  render() {
    return (
      <div className="wrapper">
        <Navbar data={this.state.data} />
        <div className="main">
          <Sidebar className="sidebar" routes={routes} {...this.props} />
          <div className="content">
            <Switch>
              {routes.map((prop, key) => {
                return (
                  <Route
                    path={prop.layout + prop.path}
                    component={prop.component}
                    key={key}
                  />
                );
              })}
            </Switch>
          </div>
        </div>
      </div>
    );
  }
}
export default AdminDashboard;
